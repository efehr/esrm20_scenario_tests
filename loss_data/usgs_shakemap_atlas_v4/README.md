# Loss Data: USGS ShakeMap Atlas

The file `losses_usgs_shakemap_atlas-v4_2000-2022.xlsx` includes modelled losses (using [ESRM20 exposure and vulnerability models](https://gitlab.seismo.ethz.ch/efehr/esrm20): Crowley et al., 2021) for 347 events that have occurred in Europe between 2000 and 2022. The ShakeMaps for events have been extracted from the [USGS ShakeMap Atlas v4](https://earthquake.usgs.gov/data/shakemap/atlas): the details of these events can be found in `models/usgs_shakemap_atlas_v4` directory. 


## References

Crowley H., Dabbeek J., Despotaki V., Rodrigues D., Martins L., Silva V., Romão, X., Pereira N., Weatherill G. and Danciu L. (2021) European Seismic Risk Model (ESRM20), EFEHR Technical Report 002, V1.0.0, [https://doi.org/10.7414/EUC-EFEHR-TR002-ESRM20](https://doi.org/10.7414/EUC-EFEHR-TR002-ESRM20)




